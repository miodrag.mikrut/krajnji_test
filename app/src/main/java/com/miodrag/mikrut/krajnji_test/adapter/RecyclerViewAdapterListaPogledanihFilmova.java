package com.miodrag.mikrut.krajnji_test.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.miodrag.mikrut.krajnji_test.R;
import com.miodrag.mikrut.krajnji_test.db.model.PogledanFilm;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecyclerViewAdapterListaPogledanihFilmova extends
        RecyclerView.Adapter<RecyclerViewAdapterListaPogledanihFilmova.MyViewHolder> {

    public List<PogledanFilm> listaPogledanihFilmova;
    public OnRecyclerItemClickListener listener;

    public interface OnRecyclerItemClickListener {
        void onRVItemClick(PogledanFilm pogledanFilm);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        TextView godina;
        ImageView slika;
        View view;

        public MyViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            slika = itemView.findViewById(R.id.rv_za_pogledani_filmovi_slika);
            tvTitle = itemView.findViewById(R.id.rv_za_pogledati_title);
            godina = itemView.findViewById(R.id.rv_pogledani_filmovi_godina);
        }

        public void bind(final PogledanFilm pogledanFilm, final OnRecyclerItemClickListener listener) {
            tvTitle.setText(pogledanFilm.getNaziv());
            godina.setText(" ("+pogledanFilm.getGodina()+")");
            Picasso.get().load(pogledanFilm.getPoster_path()).into(slika);
            Log.d("REZ", "bind: "+ pogledanFilm.getPoster_path());

            Picasso.get()
                    .load(pogledanFilm.getPoster_path())
                    .placeholder(R.drawable.placeholder_image)
                    .error(R.drawable.placeholder_image)
                    .into(slika);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRVItemClick(pogledanFilm);
                }
            });
        }
    }

    public RecyclerViewAdapterListaPogledanihFilmova(List<PogledanFilm> listaPogledanihFilmova, OnRecyclerItemClickListener listener) {
        this.listaPogledanihFilmova = listaPogledanihFilmova;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_single_item_pogledani_filmovi, viewGroup, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int i) {
        myViewHolder.bind(listaPogledanihFilmova.get(i), listener);
    }

    @Override
    public int getItemCount() {
        return listaPogledanihFilmova.size();
    }

}

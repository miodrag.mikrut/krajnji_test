package com.miodrag.mikrut.krajnji_test.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.miodrag.mikrut.krajnji_test.R;
import com.miodrag.mikrut.krajnji_test.tools.Tools;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreen extends AppCompatActivity {

    public static final int SPLASH_TIMEOUT = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        if(Tools.proveraPrefsPodesavanja("notifications",this)) {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashScreen.this, Main.class));
                    finish();
                }
            }, SPLASH_TIMEOUT);
        }else{
            startActivity(new Intent(SplashScreen.this, Main.class));
            finish();
        }
    }
}

package com.miodrag.mikrut.krajnji_test.activities;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.krajnji_test.R;
import com.miodrag.mikrut.krajnji_test.db.DatabaseHelper;
import com.miodrag.mikrut.krajnji_test.db.model.FilmZaPogledati;
import com.miodrag.mikrut.krajnji_test.db.model.PogledanFilm;
import com.squareup.picasso.Picasso;

import java.sql.SQLException;

public class DetaljiJedngFilmaZaPogledati extends AppCompatActivity {


    DatabaseHelper databaseHelper;
    int movie_id;
    FilmZaPogledati filmZaPogledati;

    private Toolbar toolbar;
    TextView opis;
    TextView naziv;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalji_jednog_filma_za_pogledati);

        setupToolbar();
        inicijalizacija();
        preuzmiIntent();
    }

    //inicijalizacija polja
    private void inicijalizacija() {
        naziv = findViewById(R.id.naziv_detalji_jednog_rezultata);
        opis = findViewById(R.id.tv_plot_za_detalji_jednog_rezultata);
        imageView = findViewById(R.id.slika_detalji_jednog_rezultata);
    }

    private void preuzmiIntent(){
       movie_id = getIntent().getIntExtra("objekat_id",1);
        try {
            filmZaPogledati = getDatabaseHelper().getFilmZaPogledatiDao().queryForId(movie_id);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        naziv.setText(filmZaPogledati.getNaziv());
        opis.setText(filmZaPogledati.getOpis());
//        imageView.setImageBitmap(BitmapFactory.decodeFile(filmZaPogledati.getPoster_path()));
        Picasso.get().load(filmZaPogledati.getPoster_path()).into(imageView);
    }


    //****  Pocetak setovanja toolbara
    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail_jednog_filma_za_pogledati, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // onOptionsItemSelected method is called whenever an item in the Toolbar is selected.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, Main.class));
                break;
            case R.id.action_close:

                PogledanFilm pogledanFilm = new PogledanFilm();
                pogledanFilm.setNaziv(filmZaPogledati.getNaziv());
                pogledanFilm.setGodina(filmZaPogledati.getGodina());
                pogledanFilm.setPoster_path(filmZaPogledati.getPoster_path());
                pogledanFilm.setOpis(filmZaPogledati.getOpis());
                try {
                    getDatabaseHelper().getPogledanFilmDao().create(pogledanFilm);
                    Toast.makeText(this, "Film " + filmZaPogledati.getNaziv()+ " dodat na listu pogledanih                             ", Toast.LENGTH_SHORT).show();
                    getDatabaseHelper().getFilmZaPogledatiDao().delete(filmZaPogledati);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                View view = findViewById(R.id.detalji_jednog_filma_za_pogledati);
                Snackbar.make(view,"Dodat je film " + filmZaPogledati.getNaziv()+ " na listu",Snackbar.LENGTH_LONG).show();
//                Toast.makeText(this, "Film" + filmZaPogledati.getNaziv()+ " obrisan sa liste", Toast.LENGTH_SHORT).show();

                startActivity(new Intent(this,ListaZaPogledati.class));

                break;

        }

        return super.onOptionsItemSelected(item);
    }

    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

}

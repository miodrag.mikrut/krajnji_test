package com.miodrag.mikrut.krajnji_test.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.krajnji_test.R;
import com.miodrag.mikrut.krajnji_test.db.DatabaseHelper;
import com.miodrag.mikrut.krajnji_test.db.model.FilmZaPogledati;
import com.miodrag.mikrut.krajnji_test.net.OMDBApiService;
import com.miodrag.mikrut.krajnji_test.net.model.Movie;
import com.miodrag.mikrut.krajnji_test.net.model.Search;
import com.squareup.picasso.Picasso;

import java.sql.SQLException;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetaljiJednogRezultata extends AppCompatActivity {

    DatabaseHelper databaseHelper;
    String movie_id;
    Movie movie;
    private Toolbar toolbar;
    RatingBar rating_star;
    TextView opis;
    TextView title;
    TextView rating;
    TextView glumac;
    TextView director;
    TextView country;
    TextView awards;
    TextView budzet;
    ImageView imageView;
    private FilmZaPogledati filmZaPogledati;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalji_jednog_rezultata);

        setupToolbar();
        inicijalizacija();
        preuzmiIntent();

        callService(movie_id);
    }

    private void inicijalizacija(){
        opis = findViewById(R.id.tv_plot_za_detalji_jednog_rezultata);
        title = findViewById(R.id.naziv_detalji_jednog_rezultata);
        imageView = findViewById(R.id.slika_detalji_jednog_rezultata);
        glumac = findViewById(R.id.glumac_detallji_jednog_rezultata);
        budzet = findViewById(R.id.budzet_detallji_jednog_rezultata);
        director = findViewById(R.id.rezija_detallji_jednog_rezultata);
        country = findViewById(R.id.drzava_detallji_jednog_rezultata);
        awards = findViewById(R.id.nagrade_detallji_jednog_rezultata);
        rating_star = findViewById(R.id.ratingbar_detalji_jednog_rezultata);
        rating = findViewById(R.id.rating_detalji_jednog_rezultata);
    }

    private void preuzmiIntent(){
        //ovde sam poslao ceo objekat search preko intenta.
        Search search = (Search) getIntent().getSerializableExtra("search");
        movie_id = search.getImdbID();
    }

    private void callService(String query){
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("apikey", "5b8913c4");
        queryParams.put("i", query); //vraca film za ID koji je uzet iz intenta koji je prosledjen u MainActivity

        Call<Movie> callMovie = OMDBApiService.apiInterface().searchMovieOMDB(queryParams);
        callMovie.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                if (!response.isSuccessful()){
//
                    Toast.makeText(DetaljiJednogRezultata.this, "Greska pri ucitavanju", Toast.LENGTH_SHORT).show();

                }else{
                    movie = response.body();
                    title.setText(movie.getTitle());
                    opis.setText(movie.getPlot());
                    glumac.setText("Glumci: "+movie.getActors());
                    budzet.setText("Budzet: "+movie.getBoxOffice());
                    director.setText("Rezija: "+movie.getDirector());
                    country.setText("Drzava: "+movie.getCountry());
                    awards.setText("Nagrade:" +movie.getAwards());
                    rating.setText("Rating: "+ movie.getImdbRating());
                    rating_star.setRating(Float.valueOf(movie.getImdbRating()));

                    Picasso.get().load(movie.getPoster()).into(imageView);
                    Log.d("REZ", "onResponse: "+ movie.getBoxOffice()+ movie.getGenre());
                }
            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
                Toast.makeText(DetaljiJednogRezultata.this,"Greska: "+t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }



    //****  Pocetak setovanja toolbara
    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // onOptionsItemSelected method is called whenever an item in the Toolbar is selected.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, Main.class));
                break;

            case R.id.action_pogledati:
                add();

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void add() {
        filmZaPogledati = new FilmZaPogledati();
        filmZaPogledati.setNaziv(title.getText().toString().trim());
        filmZaPogledati.setOpis(opis.getText().toString().trim());
        filmZaPogledati.setPoster_path(movie.getPoster());
        filmZaPogledati.setGodina(movie.getYear());
        Log.d("REZ", "add: "+movie.getPoster());
        try {
            getDatabaseHelper().getFilmZaPogledatiDao().create(filmZaPogledati);
            Toast.makeText(this, "Film je dodat na listu", Toast.LENGTH_SHORT).show();

        } catch (SQLException e) {
            e.printStackTrace();
        }


        Intent intent1 = new Intent(DetaljiJednogRezultata.this, ListaZaPogledati.class);
//                intent1.putExtra("objekat_id", movie_id);
        startActivity(intent1);

    }


    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }


}

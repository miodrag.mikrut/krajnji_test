package com.miodrag.mikrut.krajnji_test.activities;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.krajnji_test.R;
import com.miodrag.mikrut.krajnji_test.adapter.MojRecyclerViewAdapterListaFilmova;
import com.miodrag.mikrut.krajnji_test.db.DatabaseHelper;
import com.miodrag.mikrut.krajnji_test.net.OMDBApiService;
import com.miodrag.mikrut.krajnji_test.net.model.Search;
import com.miodrag.mikrut.krajnji_test.net.model.SearchResult;

import java.io.Serializable;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main extends AppCompatActivity implements Serializable,MojRecyclerViewAdapterListaFilmova.OnRecyclerItemClickListener
{
        RecyclerView rvLista;
        Toolbar toolbar;
        DatabaseHelper databaseHelper;
        Search search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupToolbar();

        rvLista = findViewById(R.id.rv_lista);
        final EditText query = findViewById(R.id.searchText);
        Button searchBtn = findViewById(R.id.searchBtn);

        //klikom na dugme za trazenje filma poziva se metoda callService sa parametrom koji je uneo korisnik
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Unese Batman -> query.getText().toString().trim()
                callService(query.getText().toString().trim());
            }
        });
    }

    private void callService(final String query){
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("apikey", "5b8913c4");
        queryParams.put("s", query);

        Call<SearchResult> call = OMDBApiService.apiInterface().searchOMDB(queryParams);
        call.enqueue(new Callback<SearchResult>() {
            @Override
            public void onResponse(Call<SearchResult> call, Response<SearchResult> response) {
                if (response.code() == 200){
                    SearchResult resp = response.body();

                    if(resp.getSearch() != null) {
                        rvLista.setLayoutManager(new LinearLayoutManager(Main.this));
                        rvLista.setAdapter(new MojRecyclerViewAdapterListaFilmova(resp.getSearch(), Main.this));
                    }else{
                        Toast.makeText(Main.this, "Za trazeni pojam "+ query + " nema rezultata", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(Main.this, "Greska pri ucitavanju je:" + response.code(),
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SearchResult> call, Throwable t) {
                Toast.makeText(Main.this,"Greska: "+t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

        //****  Pocetak setovanja toolbara
    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

        // onOptionsItemSelected method is called whenever an item in the Toolbar is selected.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, Main.class));
                break;

            case R.id.action_pogledati:
                startActivity(new Intent(Main.this,ListaZaPogledati.class));

                break;
            case R.id.action_settings:
                startActivity(new Intent(Main.this,SettingsActivity.class));

                break;
            case R.id.action_pogledani_filmovi:
                startActivity(new Intent(Main.this,ListaPogledanihFilmova.class));

                break;
        }

        return super.onOptionsItemSelected(item);
    }


    //    interfejs za komunikaciju RecyclerView i MainActivity
    @Override
    public void onRVItemClick(Search search) {
        Intent intent = new Intent(Main.this, DetaljiJednogRezultata.class);
        //saljmo ceo objekat preko intenta tako sto se implementira interfejs Serializable
        // (bolje uraditi sa Parceable) u MainActivity i
        //u klasu tog objekta koji saljemo
        intent.putExtra("search",search);

        startActivity(intent);
    }

    //***** za komuikaciju sa bazom i oslobadjanje resursa
    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
    //***** KRAJ za komuikaciju sa bazom i oslobadjanje resursa

}



package com.miodrag.mikrut.krajnji_test.net;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OMDBApiService {
    /**
     * Prvo je potrebno da definisemo retrofit instancu preko koje ce komunikacija ici
     * */
//
//    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);



    static OkHttpClient okHttpClient = new OkHttpClient();

    public static Retrofit getRetrofitInstance(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Contract.BASE_URL)
                .client(okHttpClient) //stavio sam kako bih dobio neku povratnu informaciju ako ima greska
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }

    /**
     * Definisemo konkretnu instancu servisa na intnerntu sa kojim
     * vrsimo komunikaciju
     * */
    public static OMDBApiEndpoint apiInterface(){
        OMDBApiEndpoint apiService = getRetrofitInstance().create(OMDBApiEndpoint.class);
        return apiService;
    }


}

package com.miodrag.mikrut.krajnji_test.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.krajnji_test.R;
import com.miodrag.mikrut.krajnji_test.adapter.MojRecyclerViewAdapterListaZaPogledati;
import com.miodrag.mikrut.krajnji_test.db.DatabaseHelper;
import com.miodrag.mikrut.krajnji_test.db.model.FilmZaPogledati;
import com.miodrag.mikrut.krajnji_test.net.model.Search;

import java.sql.SQLException;
import java.util.List;

public class ListaZaPogledati extends AppCompatActivity implements MojRecyclerViewAdapterListaZaPogledati.OnRecyclerItemClickListener{

    Toolbar toolbar;
    DatabaseHelper databaseHelper;
    private List<FilmZaPogledati> listaFilmovaZaPogledati;
    private RecyclerView recyclerView;
    private MojRecyclerViewAdapterListaZaPogledati mojRecyclerViewAdapterListaZaPogledati;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_za_pogledati);

        setupToolbar();
        setupRV();
    }

    //****  Pocetak setovanja toolbara
    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, Main.class));
                break;


        }

        return super.onOptionsItemSelected(item);
    }


    //inicijalizacija RV adatera
    private void setupRV() {
        try {
            listaFilmovaZaPogledati = getDatabaseHelper().getFilmZaPogledatiDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        recyclerView = findViewById(R.id.rv_list_filmova_za_pogledati);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mojRecyclerViewAdapterListaZaPogledati = new MojRecyclerViewAdapterListaZaPogledati(listaFilmovaZaPogledati, this);
        recyclerView.setAdapter(mojRecyclerViewAdapterListaZaPogledati);
    }

    //interfejs RecyclerView-a koji na klik na item u RV listi salje intent sa parametrom ID kliknutog objekta
    @Override
    public void onRVItemClick(FilmZaPogledati filmZaPogledati) {
        Intent intent = new Intent(ListaZaPogledati.this,DetaljiJedngFilmaZaPogledati.class);
        intent.putExtra("objekat_id",filmZaPogledati.getId());
        startActivity(intent);
        Toast.makeText(this, ""+ filmZaPogledati.getNaziv(), Toast.LENGTH_SHORT).show();
    }


    //***** za komuikaciju sa bazom i oslobadjanje resursa
    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }


    //***** KRAJ za komuikaciju sa bazom i oslobadjanje resursa


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this,Main.class));
    }
}

package com.miodrag.mikrut.krajnji_test.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.krajnji_test.R;
import com.miodrag.mikrut.krajnji_test.adapter.MojRecyclerViewAdapterListaZaPogledati;
import com.miodrag.mikrut.krajnji_test.adapter.RecyclerViewAdapterListaPogledanihFilmova;
import com.miodrag.mikrut.krajnji_test.db.DatabaseHelper;
import com.miodrag.mikrut.krajnji_test.db.model.PogledanFilm;

import java.sql.SQLException;
import java.util.List;

public class ListaPogledanihFilmova extends AppCompatActivity implements RecyclerViewAdapterListaPogledanihFilmova.OnRecyclerItemClickListener{

    Toolbar toolbar;
    DatabaseHelper databaseHelper;
    private List<PogledanFilm> listaPogledanihFilmova;
    private RecyclerView recyclerView;
    private RecyclerViewAdapterListaPogledanihFilmova recyclerViewAdapterListaPogledanihFilmova;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pogledanih_fimova);

        setupToolbar();
        setupRV();
    }

    //****  Pocetak setovanja toolbara
    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.show();
        }
        setTitle("Lista pogledanih filmova");
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, Main.class));
                break;


        }

        return super.onOptionsItemSelected(item);
    }


    //inicijalizacija RV adatera
    private void setupRV() {
        try {
            listaPogledanihFilmova = getDatabaseHelper().getPogledanFilmDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        recyclerView = findViewById(R.id.rv_lista_pogledanih_fimova);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewAdapterListaPogledanihFilmova = new RecyclerViewAdapterListaPogledanihFilmova(listaPogledanihFilmova, this);
        recyclerView.setAdapter(recyclerViewAdapterListaPogledanihFilmova);
    }

    //interfejs RecyclerView-a koji na klik na item u RV listi salje intent sa parametrom ID kliknutog objekta
    @Override
    public void onRVItemClick(PogledanFilm pogledanFilm) {
//            Intent intent = new Intent(ListaPogledanihFilmova.this,DetaljiJedngFilmaZaPogledati.class);
//            intent.putExtra("objekat_id",pogledanFilm.getId());
//            startActivity(intent);
        Toast.makeText(this, ""+ pogledanFilm.getNaziv(), Toast.LENGTH_SHORT).show();
    }


    //***** za komuikaciju sa bazom i oslobadjanje resursa
    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }


    //***** KRAJ za komuikaciju sa bazom i oslobadjanje resursa


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this,Main.class));
    }

}
